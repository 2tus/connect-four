import React, { Component } from 'react';
import './GameOver.css';

class GameOver extends Component {
    render() {
      return (
        <div id="overlay">
            <div id="winner">
                <h3>{this.props.message}</h3>
                <button id="mainButton" onClick={this.props.reset}>PLAY AGAIN</button>
            </div>
         </div>
        );
    }
}
  
export default GameOver;


