import React, { Component } from 'react';
import './Start.css';

class Start extends Component {
    render() {
      return (
        <div id="overlay">
            <div id="start">
                <h3>CONNECT FOUR</h3>
                <p>Hover over any column to drop!</p>
                <button id="mainButton" onClick={this.props.start}>PLAY</button>
            </div>
        </div>
        );
    }
}
  
export default Start;


