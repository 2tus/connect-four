import React, { Component } from 'react';
import './Board.css';
import GameOver from './GameOver.js';
import Start from './Start.js';

function getInitialState() {
    return {
        showStart: true,
        currentPlayer: 1,
        totalPin: 0,
        foundWinner: false,
        winningMessage: "",
        buttonState: true,
        gameBoard: [
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        ]
    }
};

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = getInitialState();

        this.dropPin = this.dropPin.bind(this);
        this.updateBoard = this.updateBoard.bind(this);
        this.checkWinner = this.checkWinner.bind(this);
        this.checkVertical = this.checkVertical.bind(this);
        this.checkHorizontal = this.checkHorizontal.bind(this);
        this.checkRightUp = this.checkRightUp.bind(this);
        this.checkRightDown = this.checkRightDown.bind(this);
        this.restart = this.restart.bind(this);
        this.startGame = this.startGame.bind(this);
        this.draw = this.draw.bind(this);
    }
    
    componentDidUpdate(prevProps, prevState) {

        if (!this.state.foundWinner) {
            this.checkWinner();
        }
    }

    startGame() {
        this.setState((prevState) => {
            return {
                showStart: !prevState.showStart,
                buttonState: !prevState.buttonState
            };
        });
    }

    draw () {
        this.setState((prevState) => {
            return {
                winningMessage: "IT'S A DRAW!",
                foundWinner: !prevState.foundWinner,
                buttonState: !prevState.buttonState
            };
        });
    }

    restart(){
        this.setState(() => {
            return getInitialState();
        })
    }

    dropPin(rowNum, event) {
        this.updateBoard(rowNum);
        const seconds = 0.3 * 1000;
        this.setState((prevState) => {
            return {buttonState: !prevState.buttonState};
        });
        setTimeout(() => {
            this.setState((prevState) => {
                return {buttonState: !prevState.buttonState};
            });
        }, seconds) 
    }
  
    updateBoard(rowNum) {
        let currentRow = this.state.gameBoard[rowNum];
        for (let colNum = currentRow.length-1; colNum >= 0; colNum--){
            if (currentRow[colNum] === 0) {
                this.setState((prevState) => {
                    let newBoard = [...prevState.gameBoard];
                    newBoard[rowNum][colNum] += this.state.currentPlayer;
                    let updatePin = prevState.totalPin + 1; 
                    return {
                        gameBoard: newBoard,
                        totalPin: updatePin,
                        currentPlayer: prevState.currentPlayer === 1 ? 2 : 1
                    };
                });
            return;
            } 
        }
    }
    
    checkWinner() {
        if (this.state.totalPin > 6 && this.state.totalPin < 42) {
            let board = this.state.gameBoard;
            if (this.checkHorizontal(board) ||
                this.checkVertical(board) ||
                this.checkRightUp(board) ||
                this.checkRightDown(board)) {
                this.setState((prevState) => {
                    return {
                        foundWinner: !prevState.Winner,
                        buttonState: !prevState.buttonState,
                        winningMessage: prevState.currentPlayer === 1 ? "RED WON!" : "YELLOW WON!"
                    };
                });
            }
        } else if (this.state.totalPin == 42) {
            this.draw();
        } else {
            return;
        }
    }
    
    checkHorizontal(board) {
        for (let row = 0; row <= 2 ; row++) {
            for (let col = 0; col <= 6; col++) {
                if (board[row][col] !== 0 &&
                    board[row][col] === board[row+1][col] &&
                    board[row][col] === board[row+2][col] &&
                    board[row][col] === board[row+3][col]) {
                    return true;
                }
            }
        }
        return false;
    }
    
    checkVertical (board) {
        for (let row = 0; row <= 5 ; row++) {
            for (let col = 0; col <= 3; col++) {
                if (board[row][col] !== 0 &&
                    board[row][col] === board[row][col+1] &&
                    board[row][col] === board[row][col+2] &&
                    board[row][col] === board[row][col+3]) {
                    return true;
                }
            }
        }
        return false;
    }
    
    checkRightUp (board) {
        for (let row = 0; row <= 2; row++) {
            for (let col = 0; col <= 3; col++) {
                if (board[row][col] !== 0 &&
                    board[row][col] === board[row+1][col+1] &&
                    board[row][col] === board[row+2][col+2] &&
                    board[row][col] === board[row+3][col+3]) {
                    return true;
                }
            }
        }
        return false;
    }
    
    checkRightDown (board) {
        for (let row = 3; row <= 5; row++) {
            for (let col = 0; col <= 3; col++) {
                if (board[row][col] !== 0 &&
                    board[row][col] === board[row-1][col+1] &&
                    board[row][col] === board[row-2][col+2] &&
                    board[row][col] === board[row-3][col+3]) {
                    return true;
                }
            }
        }
        return false;
    }
    
    render() {
        let printBoard = this.state.gameBoard.map((row, rowIndex) => {
            return ( 
                <div className="row" key={rowIndex}>
                    {row.map((col, colIndex) => {
                        let bgColor = col === 1 ? "yellow" : 
                        col === 2 ? "red" : "pink";
                        return (
                            <div className="col" key={colIndex} id={bgColor}></div>
                        )
                    })}
                </div>
            );
        }); 

        let printButtons = [];
        for(let i = 0; i < 6; i++) {
            printButtons.push(
                <button 
                    key={i}
                    className="DropButton"
                    onClick={this.dropPin.bind(this, i)}
                    disabled={this.state.buttonState}>
                </button>
               
            )
        }

        return (
            <div> 
                <div id="turn">
                    TURN: {this.state.currentPlayer === 1 ? "YELLOW" : " RED"}   
                </div>

                <div id="board">
                    {printBoard}
                    <div id="buttonsHolder">
                        {printButtons}
                    </div>
                    {this.state.showStart ? <Start start={this.startGame}/> : null}
                    {this.state.foundWinner ? 
                    <GameOver message={this.state.winningMessage} reset={this.restart}/> : 
                    null}

                </div>

            </div>
        );
    }
}

export default Board;