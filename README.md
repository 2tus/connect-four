## Steps to start this game:

1. cd to connect-four/my-app on your terminal
2. npm start

That's it! I also uploaded this game on my website. **Check it out [here](http://code4food.life/connect4/)**!

---

## How does this Game works?

Everytime a player drops a pin, the function **updateBoard** is being called to update the board's 2D array and appearance. If there are more than 6 pins on the board, a function **checkWinner** is being called to see whether there are four connected pins. If there are 42 pins dropped but the variable **foundWinner** is still false, then it's a draw! If a winner is found, then a small window will pop up announcing a winner. Player can then decide to start a new game.  Here are the main functions:

* **updateBoard** - This function takes in a row number, check for an emtpy column of that row starting from the highest index(6) to lowest index(0), change its value from "0" to either "1" or "2" depends on which player dropped the pin.

* **checkWinner** - This function checks to see if one of these four functions checkVertical, checkHorizontal, checkRightDown and checkRightUp returns true. If true is returned, then the variable foundWinner is changed to true.

* **checkVertical** - This function loops through row starting from 0 to 5, and column 0 to 3 in each row, then check to see if there is a "1" or "2" value in [row][column] and if it is equal to [row][column+1], [row][column+2], and [row][column+3].

* **checkHorizontal** - This function loops through row starting from 0 to 2, and column 0 to 6 in each row, then check to see if there is a "1" or "2" value in [row][column] and if it is equal to [row+1][column], [row+2][column], and [row+3][column].

* **checkRightDown** - This function loops through row starting from 0 to 2, and column 0 to 3 in each row, then check to see if there is a "1" or "2" value in [row][column] and if it is equal to [row+1][column+1], [row+2][column+2], and [row+3][column+3].

* **checkRightUp** - This function loops through row starting from 3 to 5, and column 0 to 3 in each row, then check to see if there is a "1" or "2" value in [row][column] and if it is equal to [row-1][column+1], [row-2][column+2], and [row-3][column+3].

---

## Vi's Thought/Work Process

1. **Choosing the one:**
 *But then I only know ReactJS and VanillaJS, so... ReactJS it is!*

2. **Getting ready:**
 *Since I only have a few hours to work on this project, I cloned [create-react-app](https://github.com/facebook/create-react-app) for a quick set-up.*

3. **Getting to know:**
 *I got to know Connect Four through a mini-conversation...with a pen and paper. I wrote down all the features and rules of this game and then, a pseudocode of my algorithm! I needed a 2d array that will be filled with "0", variables to keep track of current player, number of pins, game status...and functions to update the gameboard and find four connected pins vertically, horizontally, and diagonally...and reassured myself that I can finish this in a few hours.*

4. **Getting some action:**
 *I jumped right in and started coding! Happy coding!*

5. **Cry a bit:**
 *What are these errors?? Google please help!*

6. **The final step:**
 *Beautify the appearance of this game through magical CSS.*